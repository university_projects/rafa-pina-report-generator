#include "Event.hpp"

////////////////////////////////////////
Event::Event(const std::string &id, const std::string &name, const std::string &creator, const std::string &responsible,
	  		 const std::string &turnout, const std::string &equipmentDistribution) :
	  		 id(std::stoi(id)), name(name), creator(creator), responsible(responsible), turnout(std::stoi(turnout)),
	  		 equipmentDistribution(EquipmentDistribution(std::stoi(equipmentDistribution)))
{
}
