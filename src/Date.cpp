#include "Date.hpp"
#include <regex>

////////////////////////////////////////
Date::Date(const std::string &date)
{
	std::regex dateRegEx("(\\d+)-(\\d+)-(\\d+)");
	std::smatch match;

	if (std::regex_match(date, match, dateRegEx))
	{
		year = std::stoi(match [1]);
		month = std::stoi(match [2]);
		day = std::stoi(match [3]);
	}
}

////////////////////////////////////////
std::string Date::getMonthName() const
{
	switch (month)
	{
		case 1: return "Enero";
		case 2: return "Febrero";
		case 3: return "Marzo";
		case 4: return "Abril";
		case 5: return "Mayo";
		case 6: return "Junio";
		case 7: return "Julio";
		case 8: return "Agosto";
		case 9: return "Septiembre";
		case 10: return "Octubre";
		case 11: return "Noviembre";
		case 12: return "Diciembre";
	}
}

////////////////////////////////////////
bool Date::equalTo(const Date &another) const
{
	return year == another.year && month == another.month && day == another.day;
}
