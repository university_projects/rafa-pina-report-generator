# Rafa Piña's report generator (RPRG)

This software is used to back up the booked events of "Don Rafael Piña González" multipurpose room.

## Requirements

+ mysql++
+ mysqlclient
+ cmake 3.0
+ PoDoFo

## Contributors

Alberto Castro Estrada
