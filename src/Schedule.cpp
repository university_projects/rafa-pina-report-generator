#include "Schedule.hpp"

////////////////////////////////////////
Schedule::Schedule(const std::string &startDate, const std::string &endDate, const std::string &startHour, const std::string &endHour) :
		 		   startDate(startDate), endDate(endDate), startHour(std::stoi(startHour)), endHour(std::stoi(endHour))
{
}

////////////////////////////////////////
std::string Schedule::toString() const
{
	std::string result;

	// Dates
	if (startDate.equalTo(endDate))
	{
		result = std::to_string(startDate.day) + " de " + startDate.getMonthName() + " del " + std::to_string(startDate.year);
	}
	else if (startDate.month == endDate.month)
	{
		result = "Del " + std::to_string(startDate.day) + " al " + std::to_string(endDate.day) + " de " + endDate.getMonthName() +
			   " del " + std::to_string(startDate.year);
	}
	else
	{
		result = "Del " + std::to_string(startDate.day) + " de " + startDate.getMonthName() + " al " +
				 std::to_string(endDate.day) + " de " + endDate.getMonthName() + " del " + std::to_string(startDate.year);
	}

	// Time
	result += "; De " + std::to_string(startHour) + ":00 a " + std::to_string(endHour + 1) + ":00 horas";

	return result;
}
