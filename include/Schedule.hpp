#ifndef RPRG_CHEDULE_HPP
#define RPRG_CHEDULE_HPP

#include "Date.hpp"
#include <string>

struct Schedule
{
	Date startDate;
	Date endDate;
	int startHour;
	int endHour;

	Schedule(const std::string &startDate, const std::string &endDate, const std::string &startHour, const std::string &endHour);
	std::string toString() const;
};

#endif // RPRG_CHEDULE_HPP
