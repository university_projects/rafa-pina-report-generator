#ifndef RPRG_DATE_HPP
#define RPRG_DATE_HPP

#include <string>

struct Date
{
	int day;
	int month;
	int year;

	Date(const std::string &date);
	std::string getMonthName() const;
	bool equalTo(const Date &another) const;
};

#endif // RPRG_DATE_HPP
