#ifndef RPRG_EVENT_HPP
#define RPRG_EVENT_HPP

#include "Schedule.hpp"
#include <string>
#include <vector>

enum EquipmentDistribution
{
	Auditory		= 1,
	School			= 2,
	RoundTable		= 3,
	UTable			= 4,
	RussianTable	= 5,
	ImperialTable	= 6
};

struct Event
{
	int id;
	int turnout;
	EquipmentDistribution equipmentDistribution;
	std::string name;
	std::string creator;
	std::string responsible;
	std::vector <std::string> targetAudiences;
	std::vector <Schedule> schedules;

	Event(const std::string &id, const std::string &name, const std::string &creator, const std::string &responsible,
		  const std::string &turnout, const std::string &equipmentDistribution);
};

#endif // RPRG_EVENT_HPP
