#include "Event.hpp"
#include "OutputDir.h"
#include <mysql++/connection.h>
#include <mysql++/query.h>
#include <podofo/doc/PdfStreamedDocument.h>
#include <podofo/doc/PdfPage.h>
#include <podofo/doc/PdfPainter.h>
#include <iostream>
#include <string>
#include <vector>

int main()
{
	std::vector <Event> events;
	mysqlpp::Connection db("rafael_pina_gonzalez", "localhost", "root", "");

	if (! db.connected())
	{
		std::cout << "Unable to connect\n";
		db.disconnect();
		return 0;
	}

	// Get event's general info
	auto query = db.query("SELECT event_description.id, event_description.name, event_description.turnout, event_description.responsible,"
								 "event_description.equipment_distribution,"
								 "CONCAT(nick.name, '. ', user.name, ' ', user.father_last_name, ' ', user.mother_last_name) "
								 "FROM event_description "
								 "INNER JOIN user ON user.id = event_description.user_id "
								 "INNER JOIN nick ON nick.id = user.nick_id");

	if (auto result = query.use())
	{
		while (auto row = result.fetch_row())
			events.emplace_back(row [0].c_str(), row [1].c_str(), row [5].c_str(), row [3].c_str(), row [2].c_str(), row [4].c_str());
	}

	// Get target audiences and event's schedules
	for (auto &event : events)
	{
		auto query = db.query("SELECT public_type.name FROM public_type "
							  "INNER JOIN target_audience ON target_audience.public_type_id = public_type.id "
							  "WHERE target_audience.event_description_id = " + std::to_string(event.id));

		if (auto result = query.use())
			while (auto row = result.fetch_row())
				event.targetAudiences.emplace_back(row [0].c_str());

		query = db.query("SELECT event_schedule.start_date, event_schedule.end_date, event_schedule.start_hour, event_schedule.end_hour "
						 "FROM event_schedule "
						 "INNER JOIN event_description ON event_description.id = event_schedule.event_description_id "
						 "WHERE event_description.id = " + std::to_string(event.id));

		if (auto result = query.use())
			while (auto row = result.fetch_row())
				event.schedules.emplace_back(row [0].c_str(), row [1].c_str(), row [2].c_str(), row [3].c_str());
	}

	// Create the PDF
	PoDoFo::PdfStreamedDocument document(OUTPUTDIR"events-report.pdf");
	PoDoFo::PdfFont *font = document.CreateFont("DejaVu Sans");
	const double lineHeight = 14;
	const double margin = 56.69;
	const double tab = 14 * 4;
	PoDoFo::PdfPainter painter;

	for (auto &event : events)
	{
		PoDoFo::PdfPage *page = document.CreatePage(PoDoFo::PdfPage::CreateStandardPageSize(PoDoFo::ePdfPageSize_A4));
		const double y = page->GetPageSize().GetHeight() - margin;
		painter.SetPage(page);
		painter.SetFont(font);

		painter.DrawText(margin, y, reinterpret_cast <const PoDoFo::pdf_utf8 *>(("Nombre del evento: " + event.name).c_str()));
		painter.DrawText(margin, y - lineHeight, reinterpret_cast <const PoDoFo::pdf_utf8 *>(("Creado por: "  + event.creator).c_str()));
		painter.DrawText(margin, y - lineHeight * 2, reinterpret_cast <const PoDoFo::pdf_utf8 *>(("Responsable: "  +
																								  event.responsible).c_str()));
		painter.DrawText(margin, y - lineHeight * 3, reinterpret_cast <const PoDoFo::pdf_utf8 *>("Público objetivo:"));

		int lineNumber = 4;
		for (auto &targetAudience : event.targetAudiences)
		{
			painter.DrawText(margin + tab, y - lineHeight * lineNumber, reinterpret_cast <const PoDoFo::pdf_utf8 *>(targetAudience.c_str()));
			lineNumber++;
		}

		painter.DrawText(margin, y - lineHeight * lineNumber, "Horarios:");
		lineNumber++;
		for (auto &schedule : event.schedules)
		{
			painter.DrawText(margin + tab, y - lineHeight * lineNumber, schedule.toString());
			lineNumber++;
		}

		painter.FinishPage();
	}

	document.Close();

	return 0;
}
